﻿
namespace Turnament
{
    partial class CreateTema
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Txt_TeamName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ComboSelectTeam = new System.Windows.Forms.ComboBox();
            this.btn_addMember = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_firstname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Lastname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_email = new System.Windows.Forms.TextBox();
            this.Txt_cellPhone = new System.Windows.Forms.TextBox();
            this.btn_CrateMember = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.TeammemberListbox = new System.Windows.Forms.ListBox();
            this.btn_createTouenamnet = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Txt_TeamName
            // 
            this.Txt_TeamName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txt_TeamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_TeamName.Location = new System.Drawing.Point(165, 53);
            this.Txt_TeamName.Name = "Txt_TeamName";
            this.Txt_TeamName.Size = new System.Drawing.Size(262, 35);
            this.Txt_TeamName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 29);
            this.label4.TabIndex = 5;
            this.label4.Text = "TeamName";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "SelectTeam";
            this.label1.Click += new System.EventHandler(this.label4_Click);
            // 
            // ComboSelectTeam
            // 
            this.ComboSelectTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboSelectTeam.FormattingEnabled = true;
            this.ComboSelectTeam.Location = new System.Drawing.Point(165, 94);
            this.ComboSelectTeam.Name = "ComboSelectTeam";
            this.ComboSelectTeam.Size = new System.Drawing.Size(259, 37);
            this.ComboSelectTeam.TabIndex = 9;
            // 
            // btn_addMember
            // 
            this.btn_addMember.Location = new System.Drawing.Point(165, 137);
            this.btn_addMember.Name = "btn_addMember";
            this.btn_addMember.Size = new System.Drawing.Size(169, 41);
            this.btn_addMember.TabIndex = 11;
            this.btn_addMember.Text = "AddMember";
            this.btn_addMember.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "FirstName";
            this.label2.Click += new System.EventHandler(this.label4_Click);
            // 
            // txt_firstname
            // 
            this.txt_firstname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_firstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_firstname.Location = new System.Drawing.Point(187, 222);
            this.txt_firstname.Name = "txt_firstname";
            this.txt_firstname.Size = new System.Drawing.Size(236, 35);
            this.txt_firstname.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 29);
            this.label3.TabIndex = 5;
            this.label3.Text = "AddNewMember";
            this.label3.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 29);
            this.label5.TabIndex = 5;
            this.label5.Text = "LastName";
            this.label5.Click += new System.EventHandler(this.label4_Click);
            // 
            // txt_Lastname
            // 
            this.txt_Lastname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Lastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Lastname.Location = new System.Drawing.Point(187, 263);
            this.txt_Lastname.Name = "txt_Lastname";
            this.txt_Lastname.Size = new System.Drawing.Size(236, 35);
            this.txt_Lastname.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(40, 304);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "Email";
            this.label6.Click += new System.EventHandler(this.label4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 345);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 29);
            this.label7.TabIndex = 5;
            this.label7.Text = "CellPhone";
            this.label7.Click += new System.EventHandler(this.label4_Click);
            // 
            // Txt_email
            // 
            this.Txt_email.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txt_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_email.Location = new System.Drawing.Point(187, 304);
            this.Txt_email.Name = "Txt_email";
            this.Txt_email.Size = new System.Drawing.Size(236, 35);
            this.Txt_email.TabIndex = 6;
            // 
            // Txt_cellPhone
            // 
            this.Txt_cellPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txt_cellPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_cellPhone.Location = new System.Drawing.Point(187, 345);
            this.Txt_cellPhone.Name = "Txt_cellPhone";
            this.Txt_cellPhone.Size = new System.Drawing.Size(236, 35);
            this.Txt_cellPhone.TabIndex = 6;
            // 
            // btn_CrateMember
            // 
            this.btn_CrateMember.Location = new System.Drawing.Point(187, 386);
            this.btn_CrateMember.Name = "btn_CrateMember";
            this.btn_CrateMember.Size = new System.Drawing.Size(147, 41);
            this.btn_CrateMember.TabIndex = 11;
            this.btn_CrateMember.Text = "CreateMember";
            this.btn_CrateMember.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(21, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 29);
            this.label8.TabIndex = 5;
            this.label8.Text = "CreateTeam";
            this.label8.Click += new System.EventHandler(this.label4_Click);
            // 
            // TeammemberListbox
            // 
            this.TeammemberListbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeammemberListbox.FormattingEnabled = true;
            this.TeammemberListbox.ItemHeight = 31;
            this.TeammemberListbox.Location = new System.Drawing.Point(433, 62);
            this.TeammemberListbox.Name = "TeammemberListbox";
            this.TeammemberListbox.Size = new System.Drawing.Size(283, 314);
            this.TeammemberListbox.TabIndex = 12;
            // 
            // btn_createTouenamnet
            // 
            this.btn_createTouenamnet.Location = new System.Drawing.Point(569, 382);
            this.btn_createTouenamnet.Name = "btn_createTouenamnet";
            this.btn_createTouenamnet.Size = new System.Drawing.Size(147, 41);
            this.btn_createTouenamnet.TabIndex = 11;
            this.btn_createTouenamnet.Text = "CreateTournament";
            this.btn_createTouenamnet.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(433, 382);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 41);
            this.button1.TabIndex = 11;
            this.button1.Text = "DeleteSelected";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // CreateTema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 438);
            this.Controls.Add(this.TeammemberListbox);
            this.Controls.Add(this.btn_createTouenamnet);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_CrateMember);
            this.Controls.Add(this.btn_addMember);
            this.Controls.Add(this.ComboSelectTeam);
            this.Controls.Add(this.Txt_cellPhone);
            this.Controls.Add(this.txt_Lastname);
            this.Controls.Add(this.Txt_email);
            this.Controls.Add(this.txt_firstname);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Txt_TeamName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CreateTema";
            this.Text = "CreateTeam";
            this.Load += new System.EventHandler(this.CreateTema_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Txt_TeamName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboSelectTeam;
        private System.Windows.Forms.Button btn_addMember;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_firstname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_Lastname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_email;
        private System.Windows.Forms.TextBox Txt_cellPhone;
        private System.Windows.Forms.Button btn_CrateMember;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox TeammemberListbox;
        private System.Windows.Forms.Button btn_createTouenamnet;
        private System.Windows.Forms.Button button1;
    }
}
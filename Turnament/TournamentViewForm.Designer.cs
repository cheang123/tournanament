﻿
namespace Turnament
{
    partial class TournamentViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TournamentViewForm));
            this.label1 = new System.Windows.Forms.Label();
            this.LB_TournamentName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Combo_Round = new System.Windows.Forms.ComboBox();
            this.Checkbox_unplayonly = new System.Windows.Forms.CheckBox();
            this.MatchListbox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_team1Score = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_Team2Score = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "TournamenName:";
            // 
            // LB_TournamentName
            // 
            this.LB_TournamentName.AutoSize = true;
            this.LB_TournamentName.Location = new System.Drawing.Point(246, 34);
            this.LB_TournamentName.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LB_TournamentName.Name = "LB_TournamentName";
            this.LB_TournamentName.Size = new System.Drawing.Size(125, 31);
            this.LB_TournamentName.TabIndex = 0;
            this.LB_TournamentName.Text = "<NONE>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = "Round:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Combo_Round
            // 
            this.Combo_Round.FormattingEnabled = true;
            this.Combo_Round.Location = new System.Drawing.Point(252, 70);
            this.Combo_Round.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Combo_Round.Name = "Combo_Round";
            this.Combo_Round.Size = new System.Drawing.Size(287, 39);
            this.Combo_Round.TabIndex = 1;
            // 
            // Checkbox_unplayonly
            // 
            this.Checkbox_unplayonly.AutoSize = true;
            this.Checkbox_unplayonly.Location = new System.Drawing.Point(251, 119);
            this.Checkbox_unplayonly.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Checkbox_unplayonly.Name = "Checkbox_unplayonly";
            this.Checkbox_unplayonly.Size = new System.Drawing.Size(174, 35);
            this.Checkbox_unplayonly.TabIndex = 2;
            this.Checkbox_unplayonly.Text = "UnplayOnly";
            this.Checkbox_unplayonly.UseVisualStyleBackColor = true;
            this.Checkbox_unplayonly.CheckedChanged += new System.EventHandler(this.Checkbox_unplayonly_CheckedChanged);
            // 
            // MatchListbox
            // 
            this.MatchListbox.FormattingEnabled = true;
            this.MatchListbox.ItemHeight = 31;
            this.MatchListbox.Location = new System.Drawing.Point(22, 164);
            this.MatchListbox.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.MatchListbox.Name = "MatchListbox";
            this.MatchListbox.Size = new System.Drawing.Size(517, 283);
            this.MatchListbox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(565, 165);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "Teamone";
            this.label3.Click += new System.EventHandler(this.label2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(568, 222);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "Score";
            this.label4.Click += new System.EventHandler(this.label2_Click);
            // 
            // Txt_team1Score
            // 
            this.Txt_team1Score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txt_team1Score.Location = new System.Drawing.Point(689, 221);
            this.Txt_team1Score.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Txt_team1Score.Name = "Txt_team1Score";
            this.Txt_team1Score.Size = new System.Drawing.Size(176, 38);
            this.Txt_team1Score.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(568, 332);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 31);
            this.label5.TabIndex = 0;
            this.label5.Text = "TeamTwo";
            this.label5.Click += new System.EventHandler(this.label2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(572, 390);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 31);
            this.label6.TabIndex = 0;
            this.label6.Text = "Score";
            this.label6.Click += new System.EventHandler(this.label2_Click);
            // 
            // txt_Team2Score
            // 
            this.txt_Team2Score.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_Team2Score.Location = new System.Drawing.Point(693, 388);
            this.txt_Team2Score.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.txt_Team2Score.Name = "txt_Team2Score";
            this.txt_Team2Score.Size = new System.Drawing.Size(176, 38);
            this.txt_Team2Score.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(572, 283);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(158, 31);
            this.label7.TabIndex = 0;
            this.label7.Text = "------VS------";
            this.label7.Click += new System.EventHandler(this.label2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(732, 270);
            this.button1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 57);
            this.button1.TabIndex = 5;
            this.button1.Text = "Score";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // TournamentViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 473);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txt_Team2Score);
            this.Controls.Add(this.Txt_team1Score);
            this.Controls.Add(this.MatchListbox);
            this.Controls.Add(this.Checkbox_unplayonly);
            this.Controls.Add(this.Combo_Round);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LB_TournamentName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.Name = "TournamentViewForm";
            this.Text = "TournamentViewForm";
            this.Load += new System.EventHandler(this.TournamentViewForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LB_TournamentName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Combo_Round;
        private System.Windows.Forms.CheckBox Checkbox_unplayonly;
        private System.Windows.Forms.ListBox MatchListbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_team1Score;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_Team2Score;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
    }
}
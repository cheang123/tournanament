﻿
namespace Turnament
{
    partial class CreateTournamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateTournamentForm));
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_TounamentName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Combo_addTeam = new System.Windows.Forms.ComboBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.btn_createTeam = new System.Windows.Forms.Button();
            this.btn_createPrize = new System.Windows.Forms.Button();
            this.TournamentMatchListbox = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_Score = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_deleteSelected = new System.Windows.Forms.Button();
            this.PrizeLisbox = new System.Windows.Forms.ListBox();
            this.btn_createTournament = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(162, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(364, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create Tournament";
            // 
            // Txt_TounamentName
            // 
            this.Txt_TounamentName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txt_TounamentName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_TounamentName.Location = new System.Drawing.Point(43, 109);
            this.Txt_TounamentName.Name = "Txt_TounamentName";
            this.Txt_TounamentName.Size = new System.Drawing.Size(354, 35);
            this.Txt_TounamentName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(37, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 29);
            this.label4.TabIndex = 5;
            this.label4.Text = "Tournament Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "EntryFee";
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(168, 155);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(229, 35);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(38, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 29);
            this.label3.TabIndex = 7;
            this.label3.Text = "Add Team";
            // 
            // Combo_addTeam
            // 
            this.Combo_addTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Combo_addTeam.FormattingEnabled = true;
            this.Combo_addTeam.Location = new System.Drawing.Point(181, 217);
            this.Combo_addTeam.Name = "Combo_addTeam";
            this.Combo_addTeam.Size = new System.Drawing.Size(216, 37);
            this.Combo_addTeam.TabIndex = 8;
            this.Combo_addTeam.SelectedIndexChanged += new System.EventHandler(this.Combo_Round_SelectedIndexChanged);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(317, 194);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(79, 20);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "CrateNew";
            // 
            // btn_createTeam
            // 
            this.btn_createTeam.Location = new System.Drawing.Point(101, 269);
            this.btn_createTeam.Name = "btn_createTeam";
            this.btn_createTeam.Size = new System.Drawing.Size(205, 41);
            this.btn_createTeam.TabIndex = 10;
            this.btn_createTeam.Text = "AddTeam";
            this.btn_createTeam.UseVisualStyleBackColor = true;
            // 
            // btn_createPrize
            // 
            this.btn_createPrize.Location = new System.Drawing.Point(101, 316);
            this.btn_createPrize.Name = "btn_createPrize";
            this.btn_createPrize.Size = new System.Drawing.Size(205, 41);
            this.btn_createPrize.TabIndex = 10;
            this.btn_createPrize.Text = "CreatePrize";
            this.btn_createPrize.UseVisualStyleBackColor = true;
            this.btn_createPrize.Click += new System.EventHandler(this.btn_createPrize_Click);
            // 
            // TournamentMatchListbox
            // 
            this.TournamentMatchListbox.FormattingEnabled = true;
            this.TournamentMatchListbox.ItemHeight = 20;
            this.TournamentMatchListbox.Location = new System.Drawing.Point(419, 99);
            this.TournamentMatchListbox.Name = "TournamentMatchListbox";
            this.TournamentMatchListbox.Size = new System.Drawing.Size(286, 84);
            this.TournamentMatchListbox.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(413, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 29);
            this.label5.TabIndex = 5;
            this.label5.Text = "Team/Player";
            // 
            // btn_Score
            // 
            this.btn_Score.Location = new System.Drawing.Point(587, 55);
            this.btn_Score.Name = "btn_Score";
            this.btn_Score.Size = new System.Drawing.Size(118, 41);
            this.btn_Score.TabIndex = 10;
            this.btn_Score.Text = "Score";
            this.btn_Score.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(422, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 29);
            this.label6.TabIndex = 5;
            this.label6.Text = "Team/Player";
            // 
            // btn_deleteSelected
            // 
            this.btn_deleteSelected.Location = new System.Drawing.Point(587, 205);
            this.btn_deleteSelected.Name = "btn_deleteSelected";
            this.btn_deleteSelected.Size = new System.Drawing.Size(118, 41);
            this.btn_deleteSelected.TabIndex = 10;
            this.btn_deleteSelected.Text = "DeleteSelected";
            this.btn_deleteSelected.UseVisualStyleBackColor = true;
            // 
            // PrizeLisbox
            // 
            this.PrizeLisbox.FormattingEnabled = true;
            this.PrizeLisbox.ItemHeight = 20;
            this.PrizeLisbox.Location = new System.Drawing.Point(427, 253);
            this.PrizeLisbox.Name = "PrizeLisbox";
            this.PrizeLisbox.Size = new System.Drawing.Size(286, 104);
            this.PrizeLisbox.TabIndex = 11;
            // 
            // btn_createTournament
            // 
            this.btn_createTournament.Location = new System.Drawing.Point(285, 363);
            this.btn_createTournament.Name = "btn_createTournament";
            this.btn_createTournament.Size = new System.Drawing.Size(205, 41);
            this.btn_createTournament.TabIndex = 10;
            this.btn_createTournament.Text = "CreateTournament";
            this.btn_createTournament.UseVisualStyleBackColor = true;
            this.btn_createTournament.Click += new System.EventHandler(this.btn_createPrize_Click);
            // 
            // CreateTournamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 432);
            this.Controls.Add(this.PrizeLisbox);
            this.Controls.Add(this.TournamentMatchListbox);
            this.Controls.Add(this.btn_createTournament);
            this.Controls.Add(this.btn_createPrize);
            this.Controls.Add(this.btn_deleteSelected);
            this.Controls.Add(this.btn_Score);
            this.Controls.Add(this.btn_createTeam);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.Combo_addTeam);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_TounamentName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CreateTournamentForm";
            this.Text = "CreateTournamentForm";
            this.Load += new System.EventHandler(this.CreateTournamentForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_TounamentName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox Combo_addTeam;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button btn_createTeam;
        private System.Windows.Forms.Button btn_createPrize;
        private System.Windows.Forms.ListBox TournamentMatchListbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_Score;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_deleteSelected;
        private System.Windows.Forms.ListBox PrizeLisbox;
        private System.Windows.Forms.Button btn_createTournament;
    }
}
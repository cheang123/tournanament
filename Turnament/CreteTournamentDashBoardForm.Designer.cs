﻿
namespace Turnament
{
    partial class CreteTournamentDashBoardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.btn_LoadTournament = new System.Windows.Forms.Button();
            this.Comboexisting = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_CreateTournament = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(67, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(263, 29);
            this.label8.TabIndex = 8;
            this.label8.Text = "TournamentDashBoard";
            // 
            // btn_LoadTournament
            // 
            this.btn_LoadTournament.Location = new System.Drawing.Point(90, 140);
            this.btn_LoadTournament.Name = "btn_LoadTournament";
            this.btn_LoadTournament.Size = new System.Drawing.Size(215, 41);
            this.btn_LoadTournament.TabIndex = 14;
            this.btn_LoadTournament.Text = "LoadTournament";
            this.btn_LoadTournament.UseVisualStyleBackColor = true;
            // 
            // Comboexisting
            // 
            this.Comboexisting.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Comboexisting.FormattingEnabled = true;
            this.Comboexisting.Location = new System.Drawing.Point(59, 97);
            this.Comboexisting.Name = "Comboexisting";
            this.Comboexisting.Size = new System.Drawing.Size(271, 37);
            this.Comboexisting.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(54, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(287, 29);
            this.label1.TabIndex = 12;
            this.label1.Text = "LoadExisting Trounament";
            // 
            // btn_CreateTournament
            // 
            this.btn_CreateTournament.Location = new System.Drawing.Point(90, 187);
            this.btn_CreateTournament.Name = "btn_CreateTournament";
            this.btn_CreateTournament.Size = new System.Drawing.Size(215, 41);
            this.btn_CreateTournament.TabIndex = 14;
            this.btn_CreateTournament.Text = "CrateTournament";
            this.btn_CreateTournament.UseVisualStyleBackColor = true;
            // 
            // CreteTournamentDashBoardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 263);
            this.Controls.Add(this.btn_CreateTournament);
            this.Controls.Add(this.btn_LoadTournament);
            this.Controls.Add(this.Comboexisting);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.Name = "CreteTournamentDashBoardForm";
            this.Text = "CreteTournamentDashBoardForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_LoadTournament;
        private System.Windows.Forms.ComboBox Comboexisting;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_CreateTournament;
    }
}
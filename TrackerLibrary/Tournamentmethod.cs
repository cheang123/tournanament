﻿using System.Collections.Generic;

namespace TrackerLibrary
{
    public class Tournamentmethod
    {
        public string tournamentname { get; set; }
        public decimal EntryfeeP { get; set; }
        public List<Teammodel> Enteredteam { get; set; } = new List<Teammodel>();
        public List<prizemodel> prize { get; set; } = new List<prizemodel>();
        public List<List<matchupmodel>> Rounds { get; set; } = new List<List<matchupmodel>>();

    }
}

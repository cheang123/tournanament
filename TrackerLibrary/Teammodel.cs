﻿using System.Collections.Generic;

namespace TrackerLibrary
{
    public class Teammodel
    {
        public List<Personmodel> Teammember { get; set; } = new List<Personmodel>();
        public string Teamname { get; set; }

    }
}

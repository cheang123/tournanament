﻿namespace TrackerLibrary
{
    public class matchupentrymodel
    {
        /// <summary>
        /// Represen one team in the matchup
        /// </summary>
        public Teammodel Teamcompeting { get; set; }
        /// <summary>
        /// Represents matchup for this particula team.
        /// </summary>
        public double Score { get; set; }
        /// <summary>
        ///   Represents matchup That this team come from as winner 
        /// </summary>
        public matchupmodel parenmatchup { get; set; }


    }
}
